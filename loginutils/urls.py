from django.conf.urls import url
from testMain.models import testInstance

urlpatterns = [
    url(r'^$', 'django.contrib.auth.views.login',
        {'template_name' : 'login/login.html'},
        name = 'login'),

]