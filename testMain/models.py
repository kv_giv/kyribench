from django.db import models
from django.contrib.auth.models import User

class testQuestion(models.Model):
    question = models.TextField()
    option1 = models.TextField()
    option2 = models.TextField()
    option3 = models.TextField()
    option4 = models.TextField()
    options = [option1, option2, option3, option4]
    correctOption = models.IntegerField(choices=((1,1), (2,2), (3,3), (4,4)))
    tag = models.TextField()

    def __unicode__(self):
        return self.question


class testInstance(models.Model):
    testName = models.TextField()
    questions = models.ManyToManyField(testQuestion)
    testDescription = models.TextField(db_column='description')

    def __unicode__(self):
        return self.testName

class testResult(models.Model):
    test = models.ForeignKey(testInstance)
    correctAnswersPercent = models.FloatField()
    user = models.ForeignKey(User)
    testDate = models.DateTimeField(auto_now_add=True)




