from django.shortcuts import  render
from .models import testInstance, testQuestion, testResult
from django.views.generic import ListView
from django.contrib.auth.decorators import login_required


questionProviderMapping = {}

def getQuestionProvider(user):
    if user not in questionProviderMapping:
        questionProviderMapping[user] = QuestionProvider()
    return questionProviderMapping[user]

class QuestionProvider(object):
    def __init__(self):
        self.testId = None
        self.questionIdList = []
        self.correctAnswers = 0
        self.questionCount = 1

    def getNextQuestion(self):
        question = None
        if self.questionIdList:
            questionId = self.questionIdList.pop()
            question = testQuestion.objects.get(id=questionId)
        else:
            self.testId = None
            self.questionIdList = []
        return question

    def loadQuestionsId(self, testId):
        self.questionIdList = []
        self.correctAnswers = 0
        self.questionCount = 1
        test = testInstance.objects.get(id=testId)
        self.questionIdList = [question['id'] for question in test.questions.values()]
        self.questionCount = len(self.questionIdList)

    def answerQuestion(self, q_id, option):
        question = testQuestion.objects.get(id = q_id)
        print option, question.correctOption
        if option == question.correctOption:
            self.correctAnswers += 1

    def getResult(self):
        return 100*self.correctAnswers/self.questionCount

def index(request):
    results = testResult.objects.all().order_by('testDate')
    context = {'results' : results}
    return render(request, 'testMain/main.html', context)

class TestList(ListView):
    model = testInstance
    context_object_name = 'tests'
    template_name = 'testMain/tests.html'

@login_required
def userResults(request):
    testResults = testResult.objects.filter(user_id=request.user.id)
    context = {'results' : testResults}
    return render(request, 'testMain/userresults.html', context)

def testPreview(request, id):
    test = testInstance.objects.filter(id=id)[0]
    context = {'test' : test}
    return render(request, 'testMain/testview.html', context)

@login_required
def testStarted(request, id, currentQuestion = None):
    questionProvider = getQuestionProvider(request.user.get_full_name())
    questionProvider.loadQuestionsId(testId = id)
    question = questionProvider.getNextQuestion()
    if question:
        context = {'question': question, 'test_id': id}
        response = render(request, 'testMain/starttest.html', context)
        return response

@login_required
def questionAnswered(request, q_id, id):
    questionProvider = getQuestionProvider(request.user.get_full_name())
    answerSubmitted = int(request.POST['option'])
    questionProvider.answerQuestion(q_id=q_id, option=answerSubmitted)
    question = questionProvider.getNextQuestion()
    if question:
        context = {'question': question, 'test_id': id}
        response = render(request, 'testMain/starttest.html', context)
        return response
    else:
        result = questionProvider.getResult()
        context = {'result' : result}
        userId = request.user.id
        tResult = testResult(test_id=id, correctAnswersPercent=result, user_id=userId)
        tResult.save()
        response = render(request, 'testMain/testresults.html', context)
        return response









