from django.contrib import admin

from .models import testInstance, testQuestion, testResult

admin.site.register(testInstance)
admin.site.register(testQuestion)
admin.site.register(testResult)