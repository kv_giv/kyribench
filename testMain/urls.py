from django.conf.urls import patterns, url

import views

from testconst import TEST_PREVIEW_URL, TEST_STARTED_URL, QUESTION_SUBMITTED_URL

urlpatterns = [
    url(r'^$', views.index, name = 'index'),
    url(TEST_PREVIEW_URL, views.testPreview, name='testPreview'),
    url(r'tests', views.TestList.as_view(), name = 'testList'),
    url(r'profile', views.userResults, name='profile'),
    url(QUESTION_SUBMITTED_URL, views.questionAnswered, name='submitQuestion'),
    url(TEST_STARTED_URL, views.testStarted, name = 'testStart'),
]